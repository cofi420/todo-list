import TodaysTask from "./TodaysTask";
import { useState } from "react";
interface IdProps{
    id: number
    taskManager: JSON[]
    setTaskManager: (taskManagers: JSON[]) => void
    today: Date
}

function TaskInfo(idProps: IdProps){
    const [name, setName] = useState('');
    // console.log(idProps.taskManager);
    
    const handleSubmit = async (e) => {
        e.preventDefault()
        const newObject = {
          name: name,
          description: "",
          status:false,
          dateCreated: new Date(idProps.today),
          taskManagerId: idProps.id
        };
    
        try {
          const response = await fetch('http://localhost:5169/api/Task', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(newObject),
          });
          if (!response.ok) {
            throw new Error('Failed to upload object to the server');
          }
          
          let temp = await response.json()
          temp.sort((a,b) => {
            return a.id - b.id
          })
          console.log('submit: ', temp);
          
          idProps.setTaskManager(temp)
        } catch (error) {
          console.error('Error uploading object to the server:', error);
        }
      };
      

    return(
    <div className="ostatak">
        <div className="add-task">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-card-checklist" viewBox="0 0 16 16" onClick={handleSubmit}>
                <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
            </svg>
            <form onSubmit={(e) =>handleSubmit(e)}>
                <input type="text" placeholder="Add a task..." className="add-task"  onChange={(e) => setName(e.target.value)}>
                </input>
            </form>
            
        </div>
        
        <TodaysTask taskManagers={idProps.taskManager} id = {idProps.id} setTaskManagers={idProps.setTaskManager} today = {idProps.today} />
    </div>
);
}

export default TaskInfo;