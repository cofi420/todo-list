import { useState } from "react";
import TaskInfo from "./TaskInfo";

interface IdProps{
    id: number
    taskManager: JSON[]
    setTaskManager: (taskManagers: JSON[]) => void
}

function TaskManager(idProps: IdProps) {
    const [today, setToday] = useState(new Date());
    // useEffect(() => {
    //     const intervalId = setInterval(() => {
    //     setToday(new Date());
    //     }, 10000); // Update the date every second (you can adjust the interval as needed)

    //     // Clear the interval when the component unmounts to avoid memory leaks
    //     return () => clearInterval(intervalId);
    // }, []);

    const getFormattedDate = () => {
        const options = {month: 'short', day: 'numeric', year: 'numeric' };
        return today.toLocaleDateString(undefined, options);
    };
    const dateBefore = () => {
        const oneDayInMilliseconds = 24 * 60 * 60 * 1000
        const previousDayTimestamp = today.getTime() - oneDayInMilliseconds
        const previousDayDate = new Date(previousDayTimestamp);
        setToday(previousDayDate)
    }
    const dateAfter = () => {
        const oneDayInMilliseconds = 24 * 60 * 60 * 1000
        const previousDayTimestamp = today.getTime() + oneDayInMilliseconds
        const afterDayDate = new Date(previousDayTimestamp);
        // if (afterDayDate > today){
        //     return
        // }
        setToday(afterDayDate)
    }
    return(
        <div className="task-manager">
            <div className="gornji-deo">
                <div className="previous-day">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-chevron-left" viewBox="0 0 16 16" onClick={dateBefore}>
                        <path fillRule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                    </svg>
                    <p className="change-day" onClick={dateBefore}>previous day</p>
                </div>
                <div className="day-info">
                    <h3>{today.toLocaleDateString(undefined, { weekday: 'long' })}</h3>
                    <p>{getFormattedDate()}</p>
                </div>

                <div className="next-day">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-chevron-right" viewBox="0 0 16 16" onClick={dateAfter}>
                        <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                    <p className="change-day" onClick={dateAfter}>next day</p>
                </div>
            </div>
            <TaskInfo taskManager={idProps.taskManager}id = {idProps.id} setTaskManager={idProps.setTaskManager} today={today}/>
        </div>
    );
}

export default TaskManager;