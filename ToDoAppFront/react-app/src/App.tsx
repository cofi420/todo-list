// import DesnaStrana from "./DesnaStrana";
// import Sredina from "./Sredina";
import LevaStrana from "./LevaStrana";
import Page from "./Page";
import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
function App() {
  const [records, setRecords] = useState([]);
  const [user, setUser] = useState({});
  const [userId, setUserId] = useState(100)

  const postUser = async () => {
    const tempUser = {
      email:"johndoe@gmail.com",
      name:"John doe",
      toDoLists: []
    }
      try {
        const response = await fetch('http://localhost:5169/api/User', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(tempUser),
        });
        if (!response.ok) {
          throw new Error('Failed to upload object to the server');
        }
        var resp = await response.json();
        console.log("Response " + resp)
        setUser(resp);
        setUserId(resp.id)
      } catch (error) {
        console.error('Error uploading object to the server:', error);
      }
    };
  useEffect(() => {
    fetch("http://localhost:5169/api/User/" + userId)
      .then((response) => response.json())
      .then((data) => {return setUser(data);})
      .catch((err) => {setDefaultUser();console.log(err)});
  }, []);
  useEffect(() => {
    fetch("http://localhost:5169/api/TaskManager")
      .then((response) => response.json())
      .then((data) => {return setRecords(data)})
      .catch((err) => console.log(err));
  }, []);
  console.log(user)
  const setDefaultUser = () => postUser();
  
  const promeni = (temp:any) =>{
    // setTaskManager({...taskManager, allTasks: temp});
    setUser({...temp, toDoLists: temp})
    setRecords(temp)
  }
  return (
    <>
      <Routes>
        <Route path="/" element={<LevaStrana taskManagers={user.toDoLists} setTaskManagers={promeni} userId={userId}/>} />
        <Route
          path="/taskmanagers/:id"
          element={<Page taskManagers={user.toDoLists} setTaskManagers={promeni} userId = {userId}/>}
        />
      </Routes>
    </>
  );
}

export default App;
