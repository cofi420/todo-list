import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useNavigate } from 'react-router-dom'

interface Props{
  setTaskManagers: (taskManagers: JSON[]) => void
  userId: number 
}

function CreateList(props: Props) {
    const [show, setShow] = useState(false);
    const [name, setName] = useState('');

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const navigate = useNavigate();

    const handleSubmit = async () => {
        const newObject = {
            allTasks:[],
            name: name,
            userId: props.userId
          };
      
          try {
            const response = await fetch('http://localhost:5169/api/TaskManager', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(newObject),
            });
            
      
            if (!response.ok) {
              throw new Error('Failed to upload object to the server');
            }
      
            //console.log('Object uploaded successfully:', newObject);
            var temp = await response.json()
            props.setTaskManagers(temp)
            navigate("/");
          } catch (error) {
            console.error('Error uploading object to the server:', error);
          }
          setShow(false);
        };
        
        
    
  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Create new list
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create new task manager</Modal.Title>
        </Modal.Header>
        <Modal.Body><input type='text' className='add-task-manager' placeholder='Type in name...' onChange={(e) => setName(e.target.value)}></input></Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>);
    
}
export default CreateList
