import DesnaStrana from "./DesnaStrana";
import Sredina from "./Sredina";
import LevaStrana from "./LevaStrana";
import { useParams} from "react-router-dom"
import { useEffect, useState } from "react";
interface Props{
    taskManagers: JSON[]
    setTaskManagers: (taskManagers: JSON[]) => void
    userId: number
}

function Page(props: Props){
    const { id } = useParams();
    const [taskManager, setTaskManager] = useState<JSON[]>([]); 
    const promeni = (temp: any) => {
        // console.log("task manager ", taskManager);
        // const newState = Object.assign({}, taskManager);
        // newState.allTasks = temp;
        // setTaskManager(newState)
        
        setTaskManager({...taskManager, allTasks: temp});
        //console.log("Menjam State", newState);
      };

    useEffect(() => {
        fetch("http://localhost:5169/api/TaskManager/" + id)
          .then((response) => response.json())
          .then((data) => setTaskManager(data))
          .catch((err) => console.log(err));
      }, [id]);
    return(
        <div className="glavni">
            <LevaStrana taskManagers={props.taskManagers} setTaskManagers={props.setTaskManagers} userId={props.userId}/>
            <Sredina taskManager={taskManager} id={parseInt(id)} setTaskManager={promeni}/>
            <DesnaStrana taskManager={taskManager} key={id} id={parseInt(id)} />
        </div>
    );
}

export default Page;