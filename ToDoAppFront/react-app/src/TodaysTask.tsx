import NoTasks from "./NoTasks";

interface IdProps {
    id: number;
    taskManagers: JSON[]
    setTaskManagers: (taskManagers: JSON[]) => void
    today: Date
}

function TodaysTask(idProps: IdProps){
    
    const handeSelect = async (id:number, name:string, status:boolean, event) => {
        const newObject = {
            name: name,
            description: "",
            status: status,
            taskManagerId: idProps.id
          };
          try {                            //http://localhost:5169/api/Task/6
              const response =  await fetch('http://localhost:5169/api/Task/' + id /* + ID */, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(newObject),
            });
            if (!response.ok) {
              throw new Error('Failed to upload object to the server');
            }
            let temp = await response.json()
            temp.sort((a,b) => {
              return a.id - b.id
            })
            idProps.setTaskManagers(temp)
          } catch (error) {
            console.error('Error uploading object to the server:', error);
          }
    }
    const filterTasksByToday = (tasks : JSON[]) => {
      const today = new Date(idProps.today);
      today.setHours(0, 0, 0, 0);

      const tasksCreatedToday = tasks?.filter((task) => {
        const taskDate = new Date(task.dateCreated);
        taskDate.setHours(0, 0, 0, 0);
        return taskDate.getTime() === today.getTime();
      });
    
      return tasksCreatedToday;
    };
    const filterTasksByUncompleted = (tasks: JSON[]) => {
      const today = new Date(idProps.today);
      today.setHours(0,0,0,0);

      const tasksUncompleted = tasks?.filter((task) => {
        const taskDate = new Date(task.dateCreated);
        taskDate.setHours(0,0,0,0);
        return (taskDate.getTime() < today.getTime() && !task.status);
      });

      return tasksUncompleted;
    };
    
    const todaysTasks = filterTasksByToday(idProps.taskManagers?.allTasks)
    const uncompleteTasks = filterTasksByUncompleted(idProps.taskManagers?.allTasks)
    return(
    <div className="tasks-info">
        <h4>Uncomplete tasks</h4>
          <ul>
          {uncompleteTasks?.length === 0 ? <li><NoTasks/></li> : uncompleteTasks?.map((task)=>(
                  <li key = {task.id}>
                      <label className="one-task">{task.name}
                          <form> <input type="checkbox" defaultChecked={task.status} onChange={(event) =>{handeSelect(task.id, task.name, !task.status, event)}}></input></form>
                          
                      </label>
                  </li>
              ))}  
          </ul>
        <h4>Today's tasks</h4>
        <ul>
            {todaysTasks?.length === 0 ? <li><NoTasks/></li> : todaysTasks?.map((task)=>(
                <li key = {task.id}>
                    <label className="one-task">{task.name}
                        <form> <input type="checkbox" defaultChecked={task.status} onChange={(event) =>{handeSelect(task.id, task.name, !task.status, event)}}></input></form>
                        
                    </label>
                </li>
            ))}  
        </ul>
    </div>
    );
}

export default TodaysTask;