
import CreateList from "./CreateList";
import { useNavigate } from 'react-router-dom'

interface IdProps {
    taskManagers: JSON[];
    setTaskManagers: (taskManagers: JSON[]) => void
    userId: number 
}

function Lists(idProps: IdProps){
    const navigate = useNavigate();
    
    return(
        <div className="lists">
            <div className="lists-div">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-card-checklist" viewBox="0 0 16 16">
                    <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                    <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
                </svg>
                <p>Lists</p>
            </div>
            <ul className="list-group">
                {idProps.taskManagers?.map((list, index)=>(
                    <li key = {index} onClick={() => {navigate("/taskmanagers/" + list.id)}}>{list.name}</li>
                ))}
            </ul>
            <div className="create-new">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-plus-lg" viewBox="0 0 16 16">
                    <path fillRule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                </svg>
                {/* <p onClick={handleShowCreateList}>Create new list</p> */}
                <CreateList setTaskManagers={idProps.setTaskManagers} userId={idProps.userId}/>
            </div>
            
                
        </div>
    );
}

export default Lists;