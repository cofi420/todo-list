import { useState, useEffect } from "react";
import TaskManager from "./TaskManager";
interface IdProps{
    id: number;
    taskManager: JSON[];
    setTaskManager: (taskManagers: JSON[]) => void
}

function Sredina(idProps: IdProps){
    const [numberOfCompletedTasks, setNumberOfCompletedTasks] = useState(0)
    useEffect(() => {
        setNumberOfCompletedTasks(numUncompleted())
    }, [idProps.taskManager])

    const numUncompleted = () => {
        var br = 0;
        for (var id in idProps.taskManager?.allTasks){
            if (!idProps.taskManager?.allTasks[id].status){
                br++;
            }
        }
        return br;
    }

    return(
        <div className="sredina">
            <h2>Hi Jane</h2>
            {/* {numUncompleted() === 0 ?  <p>Well done! You don't have any uncompleted task</p>: <p>You have {numUncompleted()} uncompleted tasks</p>} */}
            {numberOfCompletedTasks === 0 ?  <p>Well done! You don't have any uncompleted task</p>: <p>You have {numberOfCompletedTasks} uncompleted tasks</p>}

            <TaskManager taskManager={idProps.taskManager} id = {idProps.id} setTaskManager={idProps.setTaskManager}/>  
        </div>
    );
}

export default Sredina;