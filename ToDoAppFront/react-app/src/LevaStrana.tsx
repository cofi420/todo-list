import Lists from "./Lists";
import Logo from "./Logo";
import User from "./User";

interface IdProps{
    taskManagers: JSON[];
    setTaskManagers: (taskManagers: JSON[]) => void
    userId: number
}

function LevaStrana(idProps: IdProps) {
    return (
        <div className="leva-strana">
            <Logo/>
            {/* <span className="border-bottom"></span> */}
            <User/>
            {/* <span className="border-bottom"></span>
            <span className="border-top"></span> */}
            <Lists taskManagers={idProps.taskManagers} setTaskManagers={idProps.setTaskManagers} userId={idProps.userId}/>
        </div>
    )
}

export default LevaStrana;