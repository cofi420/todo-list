interface IdProps{
    id:number;
    taskManager:JSON[]
}

function DesnaStrana(idProps:IdProps){
    const numCompleted = () => {
        var br = 0;
        for (var id in idProps.taskManager?.allTasks){
            if (idProps.taskManager?.allTasks[id].status){
                br++;
            }
            else{
            }
        }
        return br;
    }
    const numTasks = () => {
        return idProps.taskManager?.allTasks?.length;
    }
    return(
        <div className="desna-strana">
            <h3>Details</h3>
            <h4>{idProps.taskManager?.name}</h4>
            <h5>Statistics</h5>
            <p>{numTasks()} tasks</p>
            <p>{numCompleted()} completed</p>
        </div>
    );
}

export default DesnaStrana;