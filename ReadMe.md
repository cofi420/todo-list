**Project README**

---

### Getting Started Guide

Welcome to our project! Below is a step-by-step guide to help you get started with setting up the backend and frontend environments.

### Backend Setup

1. **Clone the Repository:**

   - Start by cloning the repository to your local machine using the following command:
     ```
     git clone https://gitlab.com/cofi420/todo-list
     ```

2. **Position Yourself in the Backend Directory:**

   - Navigate to the backend directory of the project using the terminal:
     ```
     cd ToDoApp
     ```

3. **Update Database:**

   - Run the following command to apply any pending migrations and update the database:
     ```
     dotnet ef database update
     ```

4. **Run the Backend Server:**
   - Start the backend server by running:
     ```
     dotnet run
     ```

### Frontend Setup

1. **Position Yourself in the Frontend Directory:**

   - Now, navigate to the frontend directory of the project:
     ```
     cd ../ToDoAppFront/react-app
     ```

2. **Install Dependencies:**

   - Install the necessary dependencies for the frontend:
     ```
     npm install
     ```

3. **Start the Development Server:**
   - Run the following command to start the development server for the frontend:
     ```
     npm run dev
     ```

### Accessing the Application

Once both backend and frontend servers are running, you can access the application through your web browser:

- Backend API: http://localhost5169
- Frontend UI: http://localhost:5173

### Contributing

Thank you for using our project! If you encounter any issues or have suggestions for improvements, please feel free to contribute by opening an issue or creating a pull request.

Happy coding! 🚀

---
