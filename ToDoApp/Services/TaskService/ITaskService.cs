using ToDoApp.DTOs;
using ToDoApp.Models;
using Task = ToDoApp.Models.Task;

namespace ToDoApp.Services.TaskService
{
    public interface ITaskService
    {
        Task<List<Task>> GetAllTasks();
        Task<Task?> GetSingleTask(int id);
        Task<List<Task>?> AddTask(TaskDto TaskDto);
        Task<List<Task>?> EditTask(int id, TaskDto Task);
        Task<bool> DeleteTask(int id);
    }
}