using ToDoApp.DTOs;
using ToDoApp.Models;
using Task = ToDoApp.Models.Task;

namespace ToDoApp.Services.TaskService 
{
    public class TaskService : ITaskService
    {   
        private readonly DataContext context;

        public TaskService(DataContext context)
        {
            this.context = context;
        }
        
        public async Task<List<Task>?> AddTask(TaskDto TaskDto)
        {
            Task Task = new Task(TaskDto.Id, TaskDto.Name, TaskDto.Description, TaskDto.DateCreated, TaskDto.Status);
            var tm = await context.TaskManagers.Include(t => t.AllTasks).FirstOrDefaultAsync(t => t.Id == TaskDto.TaskManagerId);
            var t = context.Tasks.Add(Task);
            if (tm is null){
                return null;
            }
            tm.AddTask(t.Entity);
            await context.SaveChangesAsync();
            return tm.AllTasks;
        }

        public async Task<bool> DeleteTask(int id)
        {
            var Task = await context.Tasks.FindAsync(id);
            if (Task is null){
                return false;
            }
            context.Tasks.Remove(Task);
            var changedRecoprds = await context.SaveChangesAsync();
            return changedRecoprds > 0;
        }
        
        public async Task<List<Task>?> EditTask(int id, TaskDto req)
        {
            var Task = await context.Tasks.FindAsync(id);
            if (Task is null){
                return null;
            }
            var tm = await context.TaskManagers.Include(t => t.AllTasks).FirstOrDefaultAsync(t => t.Id == req.TaskManagerId);
            if (tm is null) {
                return null;
            }
            Task.Description = req.Description;
            Task.Name = req.Name;
            Task.Status = req.Status;
            await context.SaveChangesAsync();
            return tm.AllTasks;
        }
        
        public async Task<List<Task>> GetAllTasks()
        {
            var tasks = await context.Tasks.ToListAsync();
            return tasks;
        }

        public async Task<Task?> GetSingleTask(int id)
        {
            var Task = await context.Tasks.FindAsync(id);
            if (Task is null){
                return null;
            }
            return Task;
        }
    }
}