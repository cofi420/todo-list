using ToDoApp.Models;
namespace ToDoApp.Services.UserService
{
    public class UserService : IUserService
    {   
        private static UserManager userManager = new UserManager();
        private readonly DataContext context;
        public UserService(DataContext context)
        {
            this.context = context;
        }
        //POST
        public async Task<List<User>?> AddUser(User user)
        {
            context.Users.Add(user);
            await context.SaveChangesAsync();
            return userManager.AllUsers;
        }
        //DELETE
        public async Task<List<User>?> DeleteUser(int id)
        {
            var user = await context.Users.FindAsync(id);
            if (user is null){
                return null;
            }
            context.Users.Remove(user);
            await context.SaveChangesAsync();
            return userManager.AllUsers;
        }
        //PUT
        public async Task<User?> EditUser(int id, User user1)
        {
            var user = await context.Users.FindAsync(id);
            if (user is null){
                return null;
            }
            userManager.EditUser(id, user1);
            await context.SaveChangesAsync();
            return user1;
        }

        //GET
        public async Task<List<User>> GetAllUsers()
        {
            return await context.Users.Include(t => t.ToDoLists).ThenInclude(list => list.AllTasks).ToListAsync();
        }
        //GET by ID
        public async Task<User?> GetSingleUser(int id)
        {
            var user = await context.Users.Include(t => t.ToDoLists).ThenInclude(list => list.AllTasks).FirstOrDefaultAsync(user => user.Id == id);
            if (user is null){
                return null;
            }
            return user;
        }
    }
}