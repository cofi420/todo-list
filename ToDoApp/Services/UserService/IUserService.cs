using ToDoApp.Models;
namespace ToDoApp.Services.UserService
{
    public interface IUserService
    {
        Task<List<User>> GetAllUsers();
        Task<User?> GetSingleUser(int id);
        Task<List<User>?> AddUser(User user);
        Task<User?> EditUser(int id, User user);
        Task<List<User>?> DeleteUser(int id);
    }
}