using ToDoApp.DTOs;
using ToDoApp.Models;
using TaskManager = ToDoApp.Models.TaskManager;

namespace ToDoApp.Services.TaskManagerService
{
    public interface ITaskManagerService
    {
        Task<List<TaskManager>> GetAllTaskManagers();
        Task<TaskManager?> GetSingleTaskManager(int id);
        Task<List<TaskManager>?> AddTaskManager(TaskManagerDto TaskManagerDto);
        Task<List<TaskManager>?> EditTaskManager(int id, TaskManagerDto TaskManagerDto);
        Task<bool> DeleteTaskManager(int id);
    }
}