using ToDoApp.DTOs;
using ToDoApp.Models;
using TaskManager = ToDoApp.Models.TaskManager;

namespace ToDoApp.Services.TaskManagerService
{
    public class TaskManagerService : ITaskManagerService
    {   
        private readonly DataContext context;
        public TaskManagerService(DataContext context)
        {
            this.context = context;
        }
        
        public async Task<List<TaskManager>?> AddTaskManager(TaskManagerDto TaskManagerDto)
        {
            TaskManager TaskManager = new TaskManager(TaskManagerDto.Name, TaskManagerDto.Id);
            var u = await context.Users.Include(tm => tm.ToDoLists).FirstOrDefaultAsync(tm => tm.Id == TaskManagerDto.UserId);
            var tm = context.TaskManagers.Add(TaskManager);
            if (u is null){
                return null;
            }
            u.ToDoLists.Add(tm.Entity);
            await context.SaveChangesAsync();
            return u.ToDoLists;
        }

        public async Task<bool> DeleteTaskManager(int id)
        {
            var TaskManager = await context.TaskManagers.FindAsync(id);
            if (TaskManager is null){
                return false;
            }
            context.TaskManagers.Remove(TaskManager);
            var numChanged = await context.SaveChangesAsync();
            return numChanged > 0;
        }

        public async Task<List<TaskManager>?> EditTaskManager(int id, TaskManagerDto req)
        {
            var TaskManager = await context.TaskManagers.FindAsync(id);
            if (TaskManager is null){
                return null;
            }
            var u = await context.Users.Include(tm => tm.ToDoLists).FirstOrDefaultAsync(tm => tm.Id == req.UserId);
            if (u is null){
                return null;
            }
            TaskManager.Name = req.Name;
            await context.SaveChangesAsync();
            return u.ToDoLists;
        }
        
        public async Task<List<TaskManager>> GetAllTaskManagers()
        {
            var TaskManagers = await context.TaskManagers.Include(t => t.AllTasks).ToListAsync();
            return TaskManagers;
        }

        public async Task<TaskManager?> GetSingleTaskManager(int id)
        {
            //var TaskManager = await context.TaskManagers.Include(t => t.AllTasks).ThenInclude(task => task.Tasks).FirstOrDefaultAsync(t => t.Id == id);
            var TaskManager = await context.TaskManagers.Include(t => t.AllTasks).FirstOrDefaultAsync(t => t.Id == id);
            if (TaskManager is null){
                return null;
            }
            return TaskManager;
        }
    }
}