namespace ToDoApp.Models
{
    public class Task
    {
        
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public DateTime DateCreated { get; set; }
        public bool Status{ get; set; }
        

        public Task(){
        }

        public Task(int id, string name, string description, DateTime dateCreated, bool status){
            Id = id;
            Name = name;
            Description = description;
            DateCreated = dateCreated;
            Status = status;
        }
        
    }
}