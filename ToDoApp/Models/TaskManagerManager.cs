namespace ToDoApp.Models
{
    public class TaskManagerManager
    {
        public List<TaskManager> AllTaskManagers { get; set; }
        public TaskManagerManager()
        {
            AllTaskManagers = new List<TaskManager>();
        }

        
        public void AddTaskManager(TaskManager taskManager)
        {
            AllTaskManagers.Add(taskManager);
        }

        public void RemoveTaskManager(TaskManager taskManager)
        {
            AllTaskManagers.Remove(taskManager);
        }
        public void EditTaskManager(int id, TaskManager taskManager)
        {
            RemoveTaskManager(FindTaskManagerById(id));
            AddTaskManager(taskManager);
        }
        public TaskManager? FindTaskManagerById(int id)
        {
            foreach (TaskManager taskManager in AllTaskManagers)
            {
                if (taskManager.Id == id)
                {
                    return taskManager;
                }
            }
            return null;
        }
        
    }
}
    