using System;
using System.Collections;
namespace ToDoApp.Models
{
    public class UserManager
    {
        public List<User> AllUsers { get; set; }

        public UserManager()
        {
            AllUsers = new List<User>();
        }
        public UserManager(List<User> AllUsers)
        {
            this.AllUsers = AllUsers;
        }
        public void AddUser(User user)
        {
            AllUsers.Add(user);
        }

        public void RemoveUser(User user)
        {
            AllUsers.Remove(user);
        }
        public void EditUser(int id, User user){
            RemoveUser(FindUserById(user.Id));
            AddUser(user);
        }
        public User FindUserById(int id)
        {
            foreach (User user in AllUsers)
            {
                if (user.Id == id)
                {
                    return user;
                }
            }
            return null;
            
        }
    }
}