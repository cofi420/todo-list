namespace ToDoApp.Models
{

    public class User
    {
        // Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<TaskManager> ToDoLists{ get; set; }

        // Constructors
        public User()
        {
            Id = 0;
            Name = "";
            Email = "";
            ToDoLists = new List<TaskManager>();
        }
        public User(int id, string name, string email)
        {
            Id = id;
            Name = name;
            Email = email;
            ToDoLists = new List<TaskManager>();
        }

        // Methods
        public void DisplayUserInfo()
        {
            Console.WriteLine($"User ID: {Id}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Email: {Email}");
        }
        public TaskManager? FindTaskManager(int id)
        {
            foreach (TaskManager tm in ToDoLists){
                if (tm.Id == id){
                    return tm;
                }
            }
            return null;
        }
        public List<Task> getUncompletedTasks(TaskManager tm){
            List<Task> list = new List<Task>();
            foreach (Task task in tm.AllTasks){
                if (!task.Status){
                    list.Add(task);
                }
            }
            return list;
        } 
        // public List<Task> getTodaysTasks(TaskManager tm) {
        //     List<Task> list = new List<Task>();
        //     foreach (Task task in tm.AllTasks){
        //         if(task.DateCreated.Date == DateTime.Today){
        //             list.Add(task);
        //         }
        //     }
        //     return list;
        // }
    }
}