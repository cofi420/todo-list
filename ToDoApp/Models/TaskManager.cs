using System;
using System.Collections.Generic;

namespace ToDoApp.Models
{
    public class TaskManager
    {
        public List<Task> AllTasks { get; set; } = new List<Task>();
        public string Name { get; set; } = string.Empty;
        public int Id { get; set; }
        
        
        public TaskManager()
        {
        }

        public TaskManager(string name, int id)
        {
            AllTasks = new List<Task>();
            Name = name;
            Id = id;
        }
        public void AddTask(Task task)
        {
            AllTasks.Add(task);
        }

        public void RemoveTask(Task task)
        {
            AllTasks.Remove(task);
        }
        public void EditTask(int id, Task task1)
        {
            var task = AllTasks.Find(x => x.Id == id);
            if (task is null){
                return;
            }
            task.Description = task1.Description;
            task.Name = task1.Name;
            task.Id = task1.Id;
            task.Status = task1.Status;
        }
        public Task? FindTaskById(int id)
        {
            foreach (Task task in AllTasks)
            {
                if (task.Id == id)
                {
                    return task;
                }
            }
            return null;
        }
        
        public int GetNumberOfCompletedTasks()
        {
            int br = 0;
            foreach (Task task in AllTasks)
            {
                if (task.Status)
                {
                    br++;
                }
            }
            return br;
        }
        public int GetNumberOfUncompletedTasks()
        {
            int br = 0;
            foreach (Task task in AllTasks)
            {
                if (!task.Status)
                {
                    br++;
                }
            }
            return br;
        }
        public void izvrsi(Task task)
        {
            task.Status = true;
            EditTask(task.Id, task);
        }
        
    }
}