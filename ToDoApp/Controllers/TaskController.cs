using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Services.TaskService;
using Task = ToDoApp.Models.Task;
using ToDoApp.DTOs;
using ToDoApp.Models;
using ToDoApp.Services.TaskManagerService;

namespace ToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService taskService;
        public TaskController(ITaskService taskService, IMapper mapper)
        {
            this.taskService = taskService;
            this.mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult<List<Task>>> GetAllTasks()
        {
            return await taskService.GetAllTasks();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Task>> GetSingleTask(int id)
        {
            var task = await taskService.GetSingleTask(id);
            if (task is null){
                return NotFound("Task not found");
            }
            return Ok(task);
        }
        [HttpPost]
        public async Task<ActionResult<Task>> AddTask(TaskDto taskDto)
        {
            var retList = await taskService.AddTask(taskDto);
            return Ok(retList);
        }
        private readonly IMapper mapper;
        [HttpPut("{id}")]
        public async Task<ActionResult<List<Task>>> EditTask(int id, TaskDto task)
        {
            var tasks = await taskService.EditTask(id, task);
            if (tasks is null){
                return NotFound("Task not found");
            }
            return Ok(tasks);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<Task>>> DeleteTask(int id)
        {
            var isSuccess = await taskService.DeleteTask(id);
            if (!isSuccess){
                return NotFound("Task not found");
            }
            return NoContent();
        }
        
    }
}