using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using ToDoApp.Services.TaskManagerService;
using ToDoApp.Models;
using TaskManager = ToDoApp.Models.TaskManager;
using ToDoApp.DTOs;

namespace ToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskManagerController : ControllerBase
    {
        private readonly ITaskManagerService TaskManagerService;
        public TaskManagerController(ITaskManagerService TaskManagerService)
        {
            this.TaskManagerService = TaskManagerService;
        }
        [HttpGet]
        public async Task<ActionResult<List<TaskManager>>> GetAllTaskManagers()
        {
            return await TaskManagerService.GetAllTaskManagers();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskManager>> GetSingleTaskManager(int id)
        {
            var TaskManager = await TaskManagerService.GetSingleTaskManager(id);
            if (TaskManager is null){
                return NotFound("TaskManager not found");
            }
            return Ok(TaskManager);
        }
        [HttpPost]
        public async Task<ActionResult<List<TaskManager>>> AddTaskManager(TaskManagerDto TaskManager)
        {
            var list = await TaskManagerService.AddTaskManager(TaskManager);
            return Ok(list);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<List<TaskManager>>> EditTaskManager(int id, TaskManagerDto TaskManagerDto)
        {
            var TaskManagers = await TaskManagerService.EditTaskManager(id, TaskManagerDto);
            if (TaskManagers is null){
                return NotFound("TaskManager not found");
            }
            return Ok(TaskManagers);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<TaskManager>>> DeleteTaskManager(int id)
        {
            var isSuccess = await TaskManagerService.DeleteTaskManager(id);
            if (!isSuccess){
                return NotFound("TaskManager not found");
            }
            return NoContent();
        }
    }
}