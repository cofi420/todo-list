using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Services.UserService;
using ToDoApp.Models;

namespace ToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        
        private readonly IUserService userService1;
        public UserController(IUserService userService)
        {
            this.userService1 = userService;
        }
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetAllUsers()
        {
            return await userService1.GetAllUsers();
            
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetSingleUser(int id)
        {
            var user = await userService1.GetSingleUser(id);
            if (user is null)
            {
                return NotFound("Nije pronadjen");
            }
            return Ok(user);
        }
        [HttpPost]
        public async Task<ActionResult<List<User>>> AddUser(User user)
        {
            await userService1.AddUser(user);
            return Ok(user);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<List<User>>> EditUser(int id, User user)
        {
            User? user1 = await userService1.EditUser(id, user);
            if (user1 is null){
                return NotFound("User not found");
            }
            return Ok(user1);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<User>>> DeleteUser(int id)
        {
            var users = await userService1.DeleteUser(id);
            if (users is null){
                return NotFound("User not found");
            }
            return Ok(users);
        }
    }
}