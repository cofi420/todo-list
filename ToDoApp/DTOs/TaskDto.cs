namespace ToDoApp.DTOs
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public DateTime DateCreated { get; set; }
        public bool Status { get; set; }
        public int TaskManagerId { get; set; }
        public TaskDto(int id, string name, string description, DateTime dateCreated, bool status, int taskManagerId)
        {
            Id = id;
            Name = name;
            Description = description;
            DateCreated = dateCreated;
            Status = status;
            TaskManagerId = taskManagerId;
        }

    }
}
