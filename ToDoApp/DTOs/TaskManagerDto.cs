namespace ToDoApp.DTOs
{
    public class TaskManagerDto
    {
        public List<ToDoApp.Models.Task> AllTasks { get; set; } = new List<ToDoApp.Models.Task>();
        public string Name { get; set; } = string.Empty;
        public int Id { get; set; }
        public int UserId { get; set; }
        public TaskManagerDto(string name, int id, int userId)
        {
            AllTasks = new List<ToDoApp.Models.Task>();
            Name = name;
            Id = id;
            UserId = userId;
        }
    }
}