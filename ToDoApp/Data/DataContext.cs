global using Microsoft.EntityFrameworkCore;
using ToDoApp.Models;
using Task = ToDoApp.Models.Task;

namespace ToDoApp.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            //optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=TodoApp;Integrated Security=true;trusted_connection=true;TrustServerCertificate=true;");
            optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=test;Encrypt=False;Trusted_Connection=True;TrustServerCertificate=true;");
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskManager> TaskManagers { get; set; }
    }
}